# -*- coding: utf-8 -*-
{
    'name': "Base for Gantt view",
    'version': "0.3",
    'author': "BusinessApps",
    'website': "http://business-apps.ru",
    'category': "Project",
    'support': "info@business-apps.ru",
    'summary': "Add base for gantt view",
    'description': "",
    'license': 'OPL-1',
    'price': 24.90,
    'currency': 'EUR',
    'images':['static/description/banner.jpg'],
    'data': [
        'views/views.xml',
    ],
    'qweb': [
        'static/src/xml/*.*',
    ],
    'depends': ['web'],
    'application': False,
}
